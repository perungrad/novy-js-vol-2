const loadAjax = (url, onSuccess, onError) => {
    console.log('> Zacinam ajax request');
    setTimeout(function () {
        const resultData = {
            error: 0,
            payload: {
                offers: [
                    {
                        id: 1,
                        position: 'Programátor',
                    },
                    {
                        id: 2,
                        position: 'UX dizajnér',
                    },
                    {
                        id: 3,
                        position: 'Projektový manažér',
                    }
                ]
            }
        };

        console.log('> Uspesne ukonceny ajax request');

        if (resultData.error === 0) {
            if (onSuccess) {
                onSuccess(resultData.payload);
            }
        } else {
            if (onError) {
                onError('Chyba na serveri: ' + resultData.error);
            }
        }
    }, 1500);
};

const displayOffers = (offers, callback) => {
    let timeout = 1000;

    console.log('');

    offers.map((offer, idx) => {
        setTimeout(
            () => {
                console.log(`| ${offer.id} | ${offer.position} |`);
            },
            timeout * (idx + 1)
        );
    });

    setTimeout(
        () => {
            console.log('');
            console.log('> Vykresleny zoznam inzeratov');

            if (callback) {
                callback();
            }
        },
        timeout * offers.length
    );
};

const displayLoader = () => {
    console.log('.......');
};

const hideLoader = () => {
    console.log('....done');
};

const showButton = () => {
    console.log('');
    console.log('< Načítaj viac ponúk >');
};

// 1. Zobrazte loader "obrázok" (`displayLoader`)
// 2. Načítajte inzeráty z URL https://api.profesia.sk/offers (`loadAjax`)
// 3. Po načítaní schovajte loader "obrázok" (`hideLoader`)
// 4.1 Ak nebola chyba:
// 4.1.1 Vykreslite zoznam inzerátov pomocou animácie (`displayOffers`)
// 4.1.2 Po vykreslení zoznamu inzerátov vykreslite tlačidlo na načítanie ďalších inzerátov (`showButton`)
// 4.2 A Ak bola chyba:
// 4.2.1 Vypíšte chybovú hlášku

